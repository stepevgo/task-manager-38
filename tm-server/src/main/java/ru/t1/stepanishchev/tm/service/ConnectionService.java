package ru.t1.stepanishchev.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.stepanishchev.tm.api.property.IDatabaseProperty;
import ru.t1.stepanishchev.tm.api.service.IConnectionService;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IDatabaseProperty databaseProperties;

    public ConnectionService(@NotNull IDatabaseProperty databaseProperties) {
        this.databaseProperties = databaseProperties;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Connection getConnection() {
        @NotNull final String username = databaseProperties.getDatabaseUsername();
        @NotNull final String password = databaseProperties.getDatabasePassword();
        @NotNull final String url = databaseProperties.getDatabaseUrl();
        @NotNull final Connection connection = DriverManager.getConnection(url, username, password);
        connection.setAutoCommit(false);
        return connection;
    }

}
