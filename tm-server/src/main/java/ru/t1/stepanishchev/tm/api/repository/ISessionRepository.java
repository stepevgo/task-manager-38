package ru.t1.stepanishchev.tm.api.repository;

import ru.t1.stepanishchev.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session>{
}
