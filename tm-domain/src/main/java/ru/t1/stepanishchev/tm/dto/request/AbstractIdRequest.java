package ru.t1.stepanishchev.tm.dto.request;

import lombok.Setter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public AbstractIdRequest(@Nullable final String token, @Nullable final String id) {
            super(token);
            this.id = id;
        }

}
